# Hosting Variables

This Aegir3 module allows you to set custom Drupal variables for each site,
such as site name and slogan.

These variables will be put in `settings.php`, and so can't be overridden
(or changed) through the site interface.

## Web Services Integration

If you have the [Aegir Services](https://www.drupal.org/project/hosting_services) module enabled, site variables can be managed remotely over a REST API.  Its Aegir SaaS submodule is especially helpful here as it does all of the set-up for you.

### Operations

#### Index

Lists all sites with Aegir-controlled variables.

* *GET* https://aegir.example.com/aegir/saas/variables.json?api-key=super-secret-key

#### Retrieve

Fetches a site's variables and their values for a particular site.

* *GET* https://aegir.example.com/aegir/saas/variables/somesite.example.com.json?api-key=super-secret-key

#### Create

Creates a site variable.

* *POST* https://aegir.example.com/aegir/saas/variables?api-key=super-secret-key
* Form data:
    * **site**: *somesite.example.com*
    * **name**: *site_name*
    * **value**: *someothersite.example.com*

#### Update

Updates a site variable.

* *PUT* https://aegir.example.com/aegir/saas/variables/somesite.example.com.json?api-key=super-secret-key
* Form data:
    * **name**: *site_name*
    * **value**: *someothersite.example.com*

#### Delete

Deletes a site variable.

* *DELETE* https://aegir.example.com/aegir/saas/variables/somesite.example.com/site_name?api-key=super-secret-key

### Notes

* See the Variables tab on a particular site to learn the Drupal 8 format for working with the configuration store and settings.
* Deleting Drupal 8 configuration settings won't be possible until [REST server fails on path ending in .xx](https://www.drupal.org/node/2808923) is resolved.
