<?php

function hosting_variables_services_index() {
  watchdog('hosting_variables', 'Remote request received: Attempting to INDEX all sites with Aegir-controlled variables.');
  return db_query("SELECT n.title FROM {node} n, {hosting_variables} v WHERE n.nid = v.site_nid")->fetchCol();
}

function hosting_variables_services_retrieve($site) {
  watchdog('hosting_variables', 'Remote request received: Attempting to RETRIEVE variables from site %site.', array(
    '%site' => $site,
  ));

  if ($site_id = hosting_get_site_by_url($site, FALSE)) {
    return hosting_variables_get_site_variables($site_id);
  }
  return array();
}

function hosting_variables_services_create($site, $name, $value) {
  watchdog('hosting_variables', 'Remote request received: Attempting to CREATE variable %variable on site %site and set it to %value.', array(
    '%variable' => $name,
    '%site' => $site,
    '%value' => $value,
  ));
  hosting_variables_services_set_variable($site, $name, $value);
}

function hosting_variables_services_update($site, $name, $value) {
  watchdog('hosting_variables', 'Remote request received: Attempting to UPDATE variable %variable on site %site and set it to %value.', array(
    '%variable' => $name,
    '%site' => $site,
    '%value' => $value,
  ));
  hosting_variables_services_set_variable($site, $name, $value);
}

function hosting_variables_services_delete($site, $name) {
  watchdog('hosting_variables', 'Remote request received: Attempting to DELETE variable %variable on site %site.', array(
    '%variable' => $name,
    '%site' => $site,
  ));

  if ($site_id = hosting_get_site_by_url($site, FALSE)) {
    $transaction = db_transaction();
    $variables = hosting_variables_get_site_variables($site_id);
    unset($variables[$name]);
    hosting_variables_set_site_variables($site_id, $variables);
  }
}

/**
 * Sets a site variable.
 *
 * This is used internally by both CREATE and UPDATE as the implementations are
 * identical.
 *
 * @see hosting_variables_services_create()
 * @see hosting_variables_services_update()
 */
function hosting_variables_services_set_variable($site, $name, $value) {
  if ($site_id = hosting_get_site_by_url($site, FALSE)) {
    $transaction = db_transaction();
    $variables = hosting_variables_get_site_variables($site_id);
    $variables[$name] = $value;
    hosting_variables_set_site_variables($site_id, $variables);
  }
}
