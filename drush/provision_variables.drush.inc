<?php

/**
* Register our directory as a place to find provision classes.
*/
function provision_variables_provision_register_autoload() {
  static $loaded = FALSE;
  if (!$loaded) {
    $loaded = TRUE;
    provision_autoload_register_prefix('Provision_', dirname(__FILE__));
  }
}

/**
* Implements hook_drush_init().
*/
function provision_variables_drush_init() {
  provision_variables_provision_register_autoload();
}

/**
* Implements hook_provision_services().
*/
function provision_variables_provision_services() {
  provision_variables_provision_register_autoload();
  return array('variables' => NULL);
}

/**
 * Implements hook_provision_drupal_config().
 */
function provision_variables_provision_drupal_config($uri, $data) {
  if ($variables = d()->hosting_variables_variables) {
    drush_log('[hosting_variables] Setting custom variables from Variables tab.');

    $lines = array();
    $lines[] = "  # Custom variables from the Variables tab.";
    foreach ($variables as $name => $value) {
      $subsystem = hosting_variables_get_subsystem($name);
      $hierarchy = hosting_variables_format_hierarchy($name);
      if (is_array($value)) {
        // Most straightforward way to have arrays in settings.php is serialize.
        $lines[] = "  \$" . $subsystem . "['" . $hierarchy . "'] =" . "unserialize('" . serialize($value) . "');";
      }
      else {
        $lines[] = "  \$" . $subsystem . "['" . $hierarchy . "'] = '" . hosting_variables_sanitize_value($value) . "';";
      }
    }
    $lines[] = "";

    return implode("\n", $lines);
  }
}

/**
 * Fetches the subsystem managing a variable.
 *
 * In Drupal 7, this is always "conf". In Drupal 8, it can either be "config"
 * (default behaviour) or "settings".
 *
 * @param string $name
 *   The variable name.
 *
 * @return string
 *   The variable subsystem.
 */
function hosting_variables_get_subsystem($name) {
  if (drush_drupal_major_version() <= 7) {
    return 'conf';
  }

  // Get the subsystem on the left of '||'.
  if (preg_match('/(.*)\|\|(.*)/', $name, $output_array)) {
    return $output_array[1];
  }
  else {
    return 'config';
  }
}

/**
 * Replace parent-child delimiters with the required array syntax.
 *
 * Tildas ("~") are used to separate parents from children in the variable hierarchy.
 *
 * @param string $name
 *   The variable name.
 * @return string
 *   The variable name with array-style delimiters.
 */
function hosting_variables_format_hierarchy($name) {
  // Remove the subsystem in case of a Drupal 8 site.
  if (drush_drupal_major_version() > 7) {
    $name = preg_replace('/(.*)\|\|(.*)/', '$2', $name);
  }

  return str_replace("~", "']['", $name);
}

/**
 * Remove or clean dangerous characters in variable values.
 *
 * @param string $value
 *   The variable value.
 * @return string
 *   The sanitized variable value.
 */
function hosting_variables_sanitize_value($value) {
  // Should we serialize everything?
  // Attempt at basic sanitation. Needs more testing.
  return str_replace("'", "\'", str_replace("\\", "", $value));
}
